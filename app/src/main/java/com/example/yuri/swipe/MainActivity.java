package com.example.yuri.swipe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "MainActivity";
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
        tv = (TextView) findViewById(R.id.tvHello);
        SwipeEvents swipeEvents = new SwipeEvents(viewGroup);
        swipeEvents.detect(new SwipeEvents.SwipeCallback() {
            @Override
            public void onSwipeTop() {
                Log.d(TAG, "Top");
                tv.setText(SwipeEvents.SwipeDirection.TOP.name());
            }

            @Override
            public void onSwipeRight() {
                Log.d(TAG, "Right");
                tv.setText(SwipeEvents.SwipeDirection.RIGHT.name());
            }

            @Override
            public void onSwipeBottom() {
                Log.d(TAG, "Bottom");
                tv.setText(SwipeEvents.SwipeDirection.BOTTOM.name());
            }

            @Override
            public void onSwipeLeft() {
                Log.d(TAG, "Left");
                tv.setText(SwipeEvents.SwipeDirection.LEFT.name());
            }

            @Override
            public void onSwipeTouch() {
                Log.d(TAG, "Touch");
                tv.setText(SwipeEvents.SwipeDirection.TOUCH.name());
            }
        });
    }
}
